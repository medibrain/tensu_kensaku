option explicit

'アーカイブ名とオリジナルファイル名を_で結合する
'ファイル名だけではなんのファイルか判別できない場合に使用

dim args,arg
dim strParentFolder
dim strPathAllFile
dim fso
dim filename

dim pattern

pattern=inputbox("0：ファイル名->フォルダ名_ファイル名" & vbcrlf & "1：ファイル名->フォルダ名")



set args=Wscript.Arguments
set fso =wscript.createobject("scripting.filesystemobject")

dim fl

for each arg in args
	strParentFolder=fso.getParentFolderName(WScript.ScriptFullName)

	for each fl in fso.getFolder(arg).Files
		'filename=fso.getFileName(arg)



		select case pattern
		case "0":
			'ファイルの親フォルダを取得
			strParentFolder=fso.getParentFolderName(fl)

			'親フォルダ名とファイル名を結合する
			filename=fl.name
			strPathAllFile=strParentFolder & "_" & filename 
		case "1":

			'ファイルの親フォルダを取得
			strParentFolder=fso.getParentFolderName(fl)
			filename=fso.getFileName(strParentFolder) & "." & fso.getExtensionName(fl)

			strPathAllFile=strParentFolder & "." & fso.getExtensionName(fl)
		end select

		'ファイルを移動する（リネーム)
		fso.movefile fl,strPathAllFile

		'親フォルダ削除
		fso.getfolder(strParentFolder).delete

	next
next
set fso=nothing

msgbox "移動終了"
