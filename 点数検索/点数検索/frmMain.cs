﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace 点数検索
{
    public partial class frm : Form
    {
        clsSQLite clsSQLite = new clsSQLite();

        #region コンストラクタ
        public frm()
        {
            InitializeComponent();
        }
        #endregion

        #region フォームロード
        private void Form1_Load(object sender, EventArgs e)
        {
            if(!clsSQLite.conn()) return;
            System.Data.DataTable dtcmb=new DataTable();

            dtcmb = clsSQLite.GetData("select name from sqlite_master where type='table'");

            cmb.DataSource = dtcmb;
            cmb.DisplayMember = "name";
            cmb.ValueMember = "name";

            cmb.SelectedIndex = 0;


        }
        #endregion

        #region 入力チェック
        private bool ChkInput()
        {
            //空の場合エラー扱い
            if (txtFind.Text == string.Empty) return false;
            if (txtSinryoYM.Text == string.Empty) return false;


            int tmp;
            if (!int.TryParse(txtSinryoYM.Text, out tmp))
            {
                MessageBox.Show("診療年月には数字を入力してください");
                return false;
            }

            return true;
        }

        #endregion

        #region 検索ボタン
        private void btnFind_Click(object sender, EventArgs e)
        {
            
            System.Text.StringBuilder sb = new StringBuilder();
            sb.Remove(0, sb.ToString().Length);

            DataGridViewCellStyle cs = new DataGridViewCellStyle();
            cs.Font = new System.Drawing.Font("ｍｓ　ゴシック", 12);
            
            //からの場合はぬけるとか、チェック機能
            if (!ChkInput()) return;

            //20201022154728 furukawa st ////////////////////////
            //try入れた
            
            try
            {
                //診療年月に合わせた改定年月を取得
                string strym;
                strym = ConvYM(this.cmb.Text);

                switch (cmb.Text)
                {

                    case "歯科診療行為":

                        sb.Remove(0, sb.ToString().Length);
                        sb.AppendLine("select 歯科診療行為コード,診療行為名称省略名称,診療行為名称基本名称,新又は現点数,変更年月日");
                        sb.AppendFormat(" from {0} where 診療行為名称基本名称 like '%{1}%' " +

                            //20191114110853 furukawa st ////////////////////////
                            //2019年10月改定に伴い、範囲検索に変更

                            "and cast(変更年月日 as int) <= {2}  order by 歯科診療行為コード",
                            //"and cast(変更年月日 as int) = {2}  order by 歯科診療行為コード",
                            //20191114110853 furukawa ed ////////////////////////

                            this.cmb.Text, this.txtFind.Text, strym);

                        this.dgv.DataSource = clsSQLite.GetData(sb.ToString());
                        if (this.dgv.DataSource == null) return;


                        dgv.Columns["歯科診療行為コード"].Width = 100;
                        dgv.Columns["診療行為名称省略名称"].Width = 100;
                        dgv.Columns["診療行為名称基本名称"].Width = 500;
                        dgv.Columns["新又は現点数"].Width = 100;
                        dgv.Columns["変更年月日"].Width = 100;

                        break;

                    case "医科診療行為":

                        sb.Remove(0, sb.ToString().Length);
                        sb.AppendLine("select 診療行為コード,省略漢字名称,基本漢字名称,新又は現点数,変更年月日");
                        sb.AppendFormat(" from {0} where 基本漢字名称 like '%{1}%' " +

                            //20191114110938 furukawa st ////////////////////////
                            //2019年10月改定に伴い、範囲検索に変更

                            "and cast(変更年月日 as int) <= {2} order by 診療行為コード ",
                            //"and cast(変更年月日 as int) = {2}  order by 診療行為コード",
                            //20191114110938 furukawa ed ////////////////////////


                            this.cmb.Text, this.txtFind.Text, strym);

                        this.dgv.DataSource = clsSQLite.GetData(sb.ToString());
                        if (this.dgv.DataSource == null) return;

                        dgv.Columns["診療行為コード"].Width = 100;
                        dgv.Columns["省略漢字名称"].Width = 100;
                        dgv.Columns["基本漢字名称"].Width = 500;
                        dgv.Columns["新又は現点数"].Width = 100;
                        dgv.Columns["変更年月日"].Width = 100;

                        break;

                    case "医薬品":

                        sb.Remove(0, sb.ToString().Length);
                        sb.AppendLine("select 医薬品コード,医薬品名漢字名称,新又は現金額,変更年月日");
                        sb.AppendFormat(" from {0} where 医薬品名漢字名称 like '%{1}%' " +


                             //20191114110938 furukawa st ////////////////////////
                             //2019年10月改定に伴い、範囲検索に変更

                             "and cast(変更年月日 as int) <= {2} order by 医薬品コード",
                             //"and cast(変更年月日 as int) = {2} order by 医薬品コード",
                             //20191114110938 furukawa ed ////////////////////////


                             this.cmb.Text, this.txtFind.Text, strym);


                        this.dgv.DataSource = clsSQLite.GetData(sb.ToString());
                        if (this.dgv.DataSource == null) return;

                        dgv.Columns["医薬品コード"].Width = 100;
                        dgv.Columns["医薬品名漢字名称"].Width = 500;
                        dgv.Columns["新又は現金額"].Width = 100;
                        dgv.Columns["変更年月日"].Width = 100;

                        break;

                    case "特定器材":

                        sb.Remove(0, sb.ToString().Length);
                        sb.AppendLine("select 特定器材コード,特定器材名_規格名_漢字名称,新又は現金額,変更年月日");
                        sb.AppendFormat(" from {0} where 特定器材名_規格名_漢字名称 like '%{1}%' " +


                            //20191114110938 furukawa st ////////////////////////
                            //2019年10月改定に伴い、範囲検索に変更

                            "and cast(変更年月日 as int) <= {2}   order by 特定器材コード",
                            //"and cast(変更年月日 as int) = {2}   order by 特定器材コード",
                            //20191114110938 furukawa ed ////////////////////////


                            this.cmb.Text, this.txtFind.Text, strym);

                        this.dgv.DataSource = clsSQLite.GetData(sb.ToString());
                        if (this.dgv.DataSource == null) return;

                        dgv.Columns["特定器材コード"].Width = 100;
                        dgv.Columns["特定器材名_規格名_漢字名称"].Width = 500;
                        dgv.Columns["新又は現金額"].Width = 100;
                        dgv.Columns["変更年月日"].Width = 100;


                        break;

                }

                dgv.DefaultCellStyle = cs;
                dgv.AutoResizeRows();
            }
            catch (Exception ex) {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
            }

            //20201022154728 furukawa ed ////////////////////////

        }
        #endregion

        #region 診療年月に適切な改定年月を取得
        private string ConvYM(string strTbl)
        {
            string strYM = string.Empty;

            System.Text.StringBuilder sb = new StringBuilder();

            int intYear=int.Parse(txtSinryoYM.Text.Substring(0, 4));
            int intMonth = int.Parse(txtSinryoYM.Text.Substring(4, 2));

            //20191114111234 furukawa st ////////////////////////
            //改定年出ない場合1年前にしていたが、年内更新が発生したので入力値そのままを使用

            //201703 ->201604
            //1～3月の場合、年を１ひく
            //if (intMonth < 4) intYear -= 1;                
            //20191114111234 furukawa ed ////////////////////////



            //201601 -> 201501 ->20140401
            //201703 -> 201603 ->20160401
            //201503 -> 201403 ->20140401
            if (intYear % 2 == 0)
            {
                //201601
                //年が偶数の場合、そのまま使う
                strYM = intYear.ToString() + "0401";
            }
            else
            {
                //年が奇数の場合、-1する
                strYM = (intYear - 1).ToString() + "0401";
            }


            //20191114111039 furukawa st ////////////////////////
            //2019年10月改定に伴い、4月1日では対応できないため変更
            
            strYM = intYear.ToString() + intMonth.ToString("00") +"01";

            //20191114111039 furukawa ed ////////////////////////

            return strYM;
            
        }
        #endregion

        #region Enterで次の項目
        private void nextfld(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }
       
        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            nextfld(e);
        }

        private void txtSinryoYM_KeyDown(object sender, KeyEventArgs e)
        {
            nextfld(e);
        }

        private void cmb_KeyDown(object sender, KeyEventArgs e)
        {
            nextfld(e);
        }
        #endregion

  
    }

    #region SQLITEクラス
    public partial class clsSQLite
    {
        SQLiteConnection cn = new SQLiteConnection();

        public bool conn()
        {
            try
            {
                cn.ConnectionString = "data source=master.db";
                cn.Open();
                return true;    
            }                       
            catch(Exception ex)
            {
                clsErr clserr = new clsErr(ex);
                return false;
            }


        }

        public void GetData(string strSQL,out string ret)
        {


            System.Data.SQLite.SQLiteCommand cmd = new SQLiteCommand();
            cmd.Connection = cn;
            cmd.CommandText = strSQL;
            ret=cmd.ExecuteScalar().ToString();
                        
        }

        public System.Data.DataTable GetData(string strSQL)
        {
            System.Data.DataTable dt = new DataTable();
            System.Data.SQLite.SQLiteCommand cmd = new SQLiteCommand();
            System.Data.SQLite.SQLiteDataAdapter da = new SQLiteDataAdapter();

            cmd.Connection = cn;
            cmd.CommandText = strSQL;

            da.SelectCommand = cmd;
            da.Fill(dt);

            if (dt.Rows.Count == 0) return null;

            return dt;
        }
    }
    #endregion

    #region エラークラス
    public partial class clsErr
    {
        public clsErr(Exception ex)
        {
            MessageBox.Show(ex.Message, Application.ProductName, 
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

    }
#endregion



}
